// console.log("Hello World");

// Array Methods

/*
	-Javascript has built-in function and methods for arrays. This allows us to manipulate and access array items.
	-Array can be either mutated or iterated
		- Array "mutations" seek to modify the contents of an array while array "iteration" aim to evaluate and loop over each element.
*/

// Mutator Methods
// functions that mutate or change an array.
// These manipulates the original array performing various task such as  adding and removing elements.

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push()
/*
	- Adds an element in the end of an array AND returns the new array's length.
	- Syntax:
		arrayName.push(newElement);
*/
console.log("Current Array: ");
console.log(fruits);
// fruits[fruits.length] = "Mango"
let fruitsLength = fruits.push("Mango"); //returns the new length of the array once we added an element.
console.log(fruitsLength);
console.log("Mutated array from push method: ");
console.log(fruits);

// Add multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method: ");
console.log(fruits);

// pop()
/*
	- Removes the last element in an array AND returns the removed element.
	-Syntax:
		arrayName.pop();

*/
let removedFruit = fruits.pop();
console.log(removedFruit); //returns removed element
console.log("Mutated array from pop method: ");
console.log(fruits);

// unshift()
/*
	- Add one or more elements at the beginning of an array.
	- Syntax:
		arrayName.unshift("elementA");
		arrayName.unshift("elementA", "elementB");
*/
fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method: ");
console.log(fruits);

// shift()
/*
	- Removes an element at the beginning of an array AND returns the removed element
	- Syntax
		arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method: ");
console.log(fruits);

// splice()
/*
	- simulatanously removes an element from a specified index number and adds new elements.
	- Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method: ");
console.log(fruits);

// sort()
/*
	-Rearranges the array elements in alphanumeric order
	-Syntax:
		- arrayName.sort();
*/

fruits.sort();
console.log("Mutated array from sort method: ");
console.log(fruits);

// reverse();
/*
	- Reverse the order of array elements
	- Syntax:
		arrayName.reverse();
*/

fruits.reverse();
console.log("Mutated array from reverse method: ");
console.log(fruits);

// Non-mutator Methods
/*
	- Non-mutator methods are functions that do not modify or change an array after they're created.
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

// indexOf()
/*
	- Returns the index of the first matching element found in an array.
	- If no match was found, the result will be -1.
	- The search process will bne done from the first element proceeding to the last element.
	- Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex/startingIndex);
*/
let firstIndex = countries.indexOf("PH");
// let firstIndex = countries.indexOf("PH", 2); //with a specific index start
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry);

// if array list is not found on indexOf() no match found returns -1 value;

// sort();

/* 


*/

// reverse();

/* 


*/
// indexOf();
/* 
	returns index number of matching element
*/

// lastIndexOf();

/* 
	- Returns the index number of the last matching element found in an array.
	- The search from process will be done fromt he last element proceeding to the first element.

	- The search from process will be done from the last element proceeding to the first element.

	- Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex/EndingIndex);

*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexStart method: " + lastIndexStart);

// slice = - Portions/slices elment form an array AND returns a new array.
/* 
	- Portions/slices element from an array and returns a new array.
	- Syntax:
		arrayName.slice(startingIndex); //until tha last element of the array.
		arrayName.slice(startingIndex, endingIndex);
*/

console.log("Original coutnries array: ");
console.log(countries);
let sliceArrayA = countries.slice(2);
// slicing of elements from specified index to the last element.
console.log("Result from slice method: " + sliceArrayA);
console.log(sliceArrayA);

// slice non mutator
// slicing off element from specified index to another index.
// But the specified last index is not included in the return.
let sliceArrayB = countries.slice(2, 5);
console.log(sliceArrayB);

// slicing off elements from last last element of an array.
let sliceArrayC = countries.slice(-1);
console.log("Result from slice method: ");
console.log(sliceArrayC);

//toString()
/* 
	- Returns an array as a string, separated by a commas.
	- Syntax 
		arrayName.toString();
*/
let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);
console.log(typeof stringArray);

// concat()

/* 
	- Combines two arrays and returns the combined result.
	- Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(element);
*/
let tasksArrayA = ["drink html", "eat javascript"];
let tasksArrayB = ["inhale css", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);

console.log("Result from concat method: ");

console.log(tasks);

// combining multiple arrays
console.log("Result from concat method: ");
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);

console.log(allTasks);

// combine arrays with elements
let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log("Result from concat method: ");
console.log(combinedTasks);

//join()

/* 
	- Returns an array as a string separated by specified separator string.
	-	Syntax
			arrayName.join("SeparatorString");


*/

let users = ["John", "Jane", "Joe", "Juan"];
console.log(users.join()); //default separated by comma
console.log(users.join(" "));
console.log(users.join(" - "));

// Iteration Methods
/* 
	- Iteration methods are loops designed to perform repetitive tasks on arrays.
	- It loops over all items/elements in an array.
*/

// forEach()
/* 
	- Similar to a for loop that iterates on each array elements.
	- For each item in the array, the anonymous function passed in the forEach() method will be run.
	- Variable names for arays are written in plurar form of the data stored.
	- It's common practice to use singular form of the array content for parameter names used in array loops.
	- forEach() does not return anything.
	- Syntax:
		arrayName.forEach(function(individualEleemnt)){
			//statement/codeblock. 
		}
*/
// allTasks.forEach(function (task) {
//   console.log(task);
// });

// Using forEach with conditional statements.
let filteredTasks = []; //store in this variables all the task names with characters greater than 10.
// anonymous function "paramater" represents each element of the array to be iterated.
allTasks.forEach(function (task) {
  // it is a good practice to print the current element in the array console when working with array methods to have idea of what information is being worked on for each iteration.
  if (task.length > 10) {
    console.log(task);
    filteredTasks.push(task);
    //   Add the element to the filteredTasks array
  }
});
console.log(filteredTasks);

// map()

/* 
	- Iterates on each element and returns new array with different values depending on the result of the function's operation.
	- This is useful for performing tasks where mutating / changing the elements required.
		-Syntax
			let/const resultArray = arrayName.map(function(individualElement));

*/

let numbers = [1, 2, 3, 4, 5];

// return the squared values of each element.

let numbersMap = numbers.map(function (number) {
  return number * number;
});
console.log("Original Array: ");
console.log(numbers); //Original is not affected by map();

console.log("Result of the map method: ");
console.log(numbersMap);

//map() vs forEach()

let numberForEach = numbers.forEach(function (number) {
  return number * number;
});
console.log(numberForEach); //undefined

//forEach(), loops over all items in the array as does map, but forEach() does not return a new array.

// every()

/* 
	- checks if all elements in an array meet the given condition
	- This is useful for validation data stored in arrays especially when dealing with large amounts of data.
	Syntax:
	let/const = resultArray = arrayName.every(function(individual element){
	return expression/expression;
	})
	// quota
*/
numbers = [1, 2, 1, 0, 2];
let allValid = numbers.every(function (number) {
  return number < 3;
});
console.log("Result of every method: ");
console.log(allValid);

// some()
/* 
	- Check if at least one element in the array meets the given conditon.
	- Returns a true if at least one element meets the condtion and false otherwise.
	- Syntax
		let/const resultArray = arrayName.some(function(individualElement){
			return expression/condition;
		})
*/
numbers = [5, 7, 9, 1, 5];
let someValid = numbers.some(function (number) {
  return number < 2;
});
console.log(someValid);

if (someValid) {
  console.log("Some numbers in the array are greater than 2.");
}

//filter()
/* 
	- Return a new array that contains elements which meets the given condition.
	- Return an empty array if no elements were found.
	- Useful for filtering array elements with given condition and shortens the  syntax.
	- Syntax:
		let/const resultArray = arrayName.filter(function(individualElement){
			return expression/condition:
		})
*/
numbers = [1, 2, 3, 4, 5];
let filterValid = numbers.filter(function (number) {
  return number < 3;
});

console.log("Result of filter method: ");
console.log(filterValid);

let nothingFound = numbers.filter(function (number) {
  return number == 0;
});
console.log("Result of filter method: ");
console.log(nothingFound);

//shorten the syntax
filteredTasks = allTasks.filter(function (task) {
  return task.length > 10;
});
console.log(filteredTasks);

// includes
/* 
	- checks if the argument passed can be found in the array.
	- it returns a boolean which can be saved in a variable.
	- Syntax:
		let/const varaiableName
		arrayName.include(<argumentToFind>);

*/
let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Monitor");
console.log("Result of includes method: ");
console.log(productFound1);

let productNotFound = products.includes("Headset");
console.log("Result of includes method: ");
console.log(productNotFound);
//filter new array
// Method chaining
// The result of the inner method is used in the outer method until all "chained" methods have been resolved.
let filteredProducts = products.filter(function (product) {
  return product.toLowerCase().includes("a");
});
console.log("Result of the chained method: ");
console.log(filteredProducts);

// reduce()

/* 
	- Evaluates elements from left and right and returns/reduces the array into a single value
	- Syntax:
		let/const resultVariable = arrayName.reduce(function(accumulator, currentValue){
			return expression/operation
		})

		- accumulator parameter stores the result for every loop.
		- currentValue parameter refers to the current/next element in the array that is evaluated in each iteration of the loop.

*/
let i = 0;
numbers = [1, 2, 3, 4, 5];
let reduceArray = numbers.reduce(function (acc, cur) {
  console.warn("Current iteration " + ++i);
  console.log("Accumlator: " + acc);
  console.log("Current Value: " + cur);
  // The operation or expression to reduce the array into a single value.
  return acc + cur;
});

console.log("Result of reduce method: " + reduceArray);

/* 
	How the "reduce" method works:
	1. The first/result element in the array is stored in the "acc" parameter
	2. The second/next element in the array is stored in the "cur" parameter
	3. An operation is performed on to the two;
	4. The loop repeats step 1-3 until all elements have been worked.
*/
